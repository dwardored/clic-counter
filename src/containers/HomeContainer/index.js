import React from 'react';
import { compose } from 'redux';

import Home from '../../views/Home';


function HomeContainer(props) {
  return (<Home {...props} />);
}


export default compose(
)(HomeContainer);

