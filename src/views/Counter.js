import React from 'react';

function Counter({
    onIncrement,
    onDecrement,
    counter,
}) {
    return (
        <div>
            <button onClick={onIncrement}>
                Vous avez cliqu� {counter} fois
      </button><button onClick={onDecrement}>
                -
      </button>
        </div>
    );
};

export default Counter;
